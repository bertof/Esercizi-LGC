\newcommand{\out}[1]{\overline{#1}}
\newcommand{\upLimP}[1]{\mathcal{O}_{P}(#1)}
\newcommand{\upLimS}[1]{\mathcal{O}_{S}(#1)}
\newcommand{\zero}{\mathbf{0}}

\chapter{Dimostrazione processo CCS finito con passi e stati finiti}
\section{Tesi}
Ogni processo CCS finito termina la sua esecuzione in un numero finito di passi.

\section{Definizioni}
\paragraph{CCS - Calculus of Communicating Systems}
È un formalismo che permette di descrivere processi concorrenti e verificarne il corretto funzionamento.
L'insieme dei processi $\mathcal{P}$ è definito
come

\[
    P, Q ::=
    \alpha.P\mid
    (P\mid Q)\mid
    \sum_{i\in I} P_i\mid
    P\setminus L\mid
    P[f]
\]

dove

\begin{itemize}
    \item \(P\) e \(Q\) sono processi;
    \item \(\alpha\in Act\) è una interazione del processo;
    \item \(\tau\) è una non-interazione;
    \item \(I\) è un insieme potenzialmente infinito di indici;
    \item \(f:Act\rightarrow Act\) è una funzione di relabelling delle interazioni tale che \(f(\tau)=\tau\) e \(f(\out{a})=\out{f(a)}\);
    \item \(L\in\mathcal{L}\) è un insieme di etichette.
\end{itemize}
Nella variante finita di CCS $\mathcal{K}$ e $Act$ sono insiemi illimitati ma finiti. \\
Il processo \(\zero\) è definito come \(\zero=\sum_{i\in \emptyset}{P_{i}}\), ha un solo stato, quello iniziale, e non ha interazioni con altri processi.

\section{Dimostrazione}
Per semplificare la dimostrazione la divido in due casi, quello con somme finite, dove l'insieme \(I\) usato nella scelta non deterministica è finito, e quello con somme infinite, dove questo vincolo viene rimosso.

\subsection{Dimostrazione per somme finite}
Per dimostrare che un processo CCS finito termina in un numero finito di passi ci basta mostrare che il numero di passi ha un limite superiore; per fare ciò possiamo utilizzare un'induzione sull'esecuzione dei processi, trovando un limite superiore a ciascun termine.

\subsubsection{Funzioni \(\mathcal{O}_{P}\) e \(\mathcal{O}_{S}\)}
Definiamo la funzione \(\mathcal{O}_{P} :\mathcal{P}\rightarrow \mathbb{N}\)
\[
    \mathcal{O}_{P}
    \begin{cases}
        P=\zero,        & 0                           \\
        P=\alpha+Q,     & 1+\upLimP{Q}                \\
        P=Q+R,          & max(\upLimP{Q}, \upLimP{R}) \\
        P=Q\mid R,      & \upLimP{Q}+\upLimP{R}       \\
        P=Q\setminus L, & \upLimP{Q}                  \\
        P=Q[f],         & \upLimP{Q}
    \end{cases}
\]
Dimostreremo che questa funzione indica il limite superiore del numero di passi eseguiti dal processo \(P\) per terminare.
Definiamo la funzione \(\mathcal{O}_{S}:\mathcal{P}\rightarrow \mathbb{N}\)
\[
    \mathcal{O}_{S}
    \begin{cases}
        P=\zero,        & 1                     \\
        P=\alpha+Q,     & 1+\upLimS{Q}          \\
        P=Q+R,          & \upLimS{Q}+\upLimS{R} \\
        P=Q\mid R,      & \upLimS{Q}*\upLimS{R} \\
        P=Q\setminus L, & \upLimS{Q}            \\
        P=Q[f],         & \upLimS{Q}
    \end{cases}
\]
Dimostreremo che questa funzione calcola il limite superiore del numero di stati dell'esecuzione del processo \(P\).

\subsubsection{Dimostrazione per induzione sull'esecuzione}
Dimostro tramite induzione sull'esecuzione dei processi che il numero di passi e di stati di esecuzione di un processo CCS finito è limitato.

\paragraph{Caso base}
Il caso base della dimostrazione è il processo vuoto \(\zero\), che ha un solo stato, quello di partenza, e non esegue passi. In questo caso, applicando le due funzioni sopra definite, \(\upLimP{\zero}=0\), mentre \(\upLimS{\zero}=1\).

\paragraph{Passo induttivo}
L'ipotesi induttiva è che un processo composto da sotto-processi con un limite superiore ai passi di esecuzione ha anch'esso un limite superiore ai passi di esecuzione. Prendiamo, dunque, come esempio i sottoprocessi \(P\) e \(Q\) che hanno limite superiore di passi rispettivamente \(\upLimP{P}\) e \(\upLimP{Q}\).

\subparagraph{\(\mathbf{\alpha.P}\)}
Il processo \(\alpha.P\), che appone l'azione \(\alpha\) al processo \(P\) aggiunge un passo, infatti l'esecuzione di \(\alpha.P\) includerà \(\alpha.P \overset{\alpha}\rightarrow P\).
Per ipotesi induttiva $P$ termina in al più \(\upLimP{P}\) passi, quindi \(\alpha.P\) terminerà al più in \(\upLimP{\alpha.P}=1+\upLimP{P}\) passi.
Allo stesso modo \(\alpha.P\) avrà un ulteriore stato durante la sua esecuzione; ponendo \(\upLimS{P}\) il limite superiore di stati dell'esecuzione di \(P\), \(\alpha.P\) esegue in \(\upLimS{\alpha.P}=1+\upLimS{P}\) stati.

\subparagraph{\(\mathbf{P+Q}\)}
Preso il processo \(P+Q\), ovvero la scelta non deterministica tra i processi \(P\) e \(Q\), poiché la sua esecuzione può procedere solo con uno di essi, possiamo dedurre che l'esecuzione del processo \(P+Q\) avrà lunghezza pari al più lungo dei due, ovvero \(\upLimP{P+Q}=max(\upLimP{P}, \upLimP{Q})\).
Gli stati di esecuzione, invece, potrebbero non essere condivisi tra i due processi, quindi i limiti superiori si sommano, dunque $\upLimS{P+Q}=\upLimS{P}+\upLimS{Q}$.

\subparagraph{\(\mathbf{P\mid Q}\)}
Il processo \(P\mid Q\), esecuzione parallela dei processi \(P\) e \(Q\), può procedere in tre modi:
\begin{itemize}
    \item \(P\mid Q\overset{\alpha}{\rightarrow}P'\mid Q\): in questo caso il sottoprocesso \(P\) ha interagito con un processo esterno, eseguendo un passo di esecuzione. Poiché il processo ha eseguito un solo passo possiamo dire che \(\upLimP{P\mid Q} = 1+\upLimP{P'\mid Q}\);
    \item \(P\mid Q\overset{\alpha}{\rightarrow}P\mid Q'\): analogo al precedente;
    \item \(P\mid Q\overset{\tau}{\rightarrow}P'\mid Q'\): in tal caso i due sottoprocessi \(P\) e \(Q\) interagiscano tra di loro, eseguendo un passo, perciò \(\upLimP{P\mid Q}=1+\upLimP{P'\mid Q'}\).
\end{itemize}
Possiamo dunque dire che \(\forall P,Q.\ \upLimP{P\mid Q}=\upLimP{P}+\upLimP{Q}\). Per quanto riguarda gli stati di esecuzione, poiché è possibile che non ci sia alcuno stato condiviso durante l'esecuzione dei due processi, il limite superiore ad essi è uguale al totale di combinazioni possibili tra gli stati dei singoli processi, dunque \(\upLimS{P\mid Q}=\upLimS{P}\times\upLimS{Q}\).

\subparagraph{\(\mathbf{P[f]}\) e \(\mathbf{P\setminus L}\)}
Applicando una funzione di relabelling ad un processo non si aumentano il massimo numero di passi né il massimo numero di stati di esecuzione, ma solo si cambiano le possibili interazioni con altri processi in parallelo. Lo stesso vale nel caso in cui si applichino restrizioni. Applicando \(\mathcal{O}_{P}\) e \(\mathcal{O}_{S}\) a \(P[f]\) e a \(P\setminus L\) troviamo rispettivamente $\upLimP{P[f]}=\upLimP{P}$ e $\upLimS{P\setminus L}=\upLimS{P}$.

\subsubsection{Dimostrazione veridicità della tesi}
Abbiamo dimostrato che le funzioni \(\mathcal{O}_{P}\) e \(\mathcal{O}_{S}\) calcolano effettivamente il limite superiore di passi di terminazione e di stati di esecuzione di un qualsiasi processo in CCS finito, mostrando, dunque, che questi sono sempre superiormente limitati; ne segue che i processi CCS finiti, definiti secondo grammatica sopra riportata, terminano in un numero di passi e di stati di esecuzione finiti.

\subsection{Dimostrazione con somme infinite}
La dimostrazione è simile a quella precedente tranne per il punto riguardante le somme: per dimostrare la finitezza dell'esecuzione dobbiamo utilizzare un'astrazione basata sulle generazioni della grammatica di CCS.

\subsubsection{Dimostrazione per induzione sulla lunghezza della derivazione}

\paragraph{Caso base}
Il caso base della dimostrazione è il termine finale, ovvero il processo \(\zero\). Abbiamo già mostrato che tale processo per definizione non esegue alcun passo, quindi termina in un numero finito di passi.

\paragraph{Passo induttivo}
La generazione della sequenza di interazioni procede tramite uno dei termini della grammatica e ha derivazione finita.

\subparagraph{Casi \(\mathbf{\alpha.P}\), \(\mathbf{P[f]}\) e \(\mathbf{P\setminus L}\)}
Nel caso di \(\mathbf{\alpha.P}\) viene apposta l'interazione \(\alpha\) al processo \(P\), dunque la derivazione produrrà un processo del tipo \(\alpha^{n+1}.P\) con \(n \geq 0\) che introduce un nuovo passo e un nuovo stato all'esecuzione di \(P\); questo implica che il numero di passi e di stati di esecuzione del processo rimane finito. Lo stesso vale per i casi di \(\mathbf{P[f]}\) e \(\mathbf{P\setminus L}\), dove la derivazione della grammatica produrrà una ripetizione finita di relabelling o restriction, dunque sempre processi con passi e stati finiti.

\subparagraph{Caso \(\mathbf{P\mid Q}\)}
Il caso dell'esecuzione parallela di \(P\) e \(Q\) produce due sottosequenze, legate ciascuna ad uno dei due sottoprocessi. Poiché per ipotesi induttiva abbiamo che i sottoprocessi utilizzati hanno una derivazione finita, la derivazione del processo sarà anch'essa finita, come mostrato nel caso di somme finite.

\subparagraph{Caso \(\mathbf{\sum_{i\in I}P_i}\)}
Questo caso pone che il processo derivato sarà del tipo \(P_1+P_2+P_3+\ldots\).
Per ipotesi induttiva ciascun \(P_i\) ha una derivazione di lunghezza finita, dunque potrei dimostrare che la derivazione di \(P\) è finita per induzione sul numero di sottoprocessi che vengono usati nella somma. \\
Il caso base è il processo \(\zero\), che già sappiamo essere finito.
L'ipotesi induttiva è che la scelta non deterministica tra i processi \(\{O_1,\ldots,P_n\}\) ha una derivazione di lunghezza finita.
Aggiungendo il processo \(P_{n+1}\) alla scelta diventa quella tra l'esecuzione di uno dei processi precedentemente presenti e \(P_{n+1}\), e poiché anche \(P_{n+1}\) ha derivazione di lunghezza finita e solo uno dei processi verrà eseguito la derivazione avrà lunghezza superiormente limitata dalla massima lunghezza di derivazione dei sotto processi.
In questo modo abbiamo dimostrato che l'insieme dei processi della scelta non deterministica può essere illimitato ma finito senza perdere la finitezza di esecuzione e poiché la derivazione esegue solo uno dei processi della scelta, abbiamo la certezza che l'altezza della derivazione di \(P\) è \(1\) sommato all'altezza della derivazione del processo scelto, ovvero \(1+max_h(P_i)\).
Per quanto riguarda, invece, il numero di stati osservati durante l'esecuzione, questo non è più superiormente limitato, infatti, assumendo che ciascuno dei sottoprocessi raggiunga un numero \(\upLimS{P_i}\) di stati, il limite superiore sul numero di stati di esecuzione è \(\sum_{i=0}^{\infty}\upLimS{P_i}\), che è chiaramente infinito.

\section{Conclusioni}
Abbiamo dimostrato che un processo CCS finito termina in passi finiti anche nel caso si utilizzi la grammatica che permette scelte non deterministiche in un insieme infinito, dunque la tesi è vera.
Possiamo, inoltre, notare come non sia più vero che anche il numero di stati di esecuzione è finito: poiché la scelta non deterministica è su un numero infinito di elementi, possibilmente diversi da \(\zero\), allora il limite superiore di stati di esecuzione sarà la somma di stati di infiniti processi, quindi anch'essa infinita.
